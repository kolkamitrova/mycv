FROM nginx:latest

LABEL maintainer="kolkamitrova@gmail.com"

COPY ./index.html /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]

EXPOSE 80
